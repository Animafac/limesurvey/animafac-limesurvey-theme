/*jslint browser: true*/
/*global $*/
$(window).load(
    function () {
        'use strict';
        $('.control-label').each(
            function () {
                if ($(this).text().trim().endsWith('(complet)')) {
                    $(this).parent().find('input').prop('disabled', true);
                }
            }
        );
    }
);
